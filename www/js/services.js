angular.module('starter.services', [])

// Login Cupo-Colegio

// Login redes sociales

// consultas datos.gov.com
.factory('DatosAbiertos',
function(
  $q,
  $http,
  $httpParamSerializerJQLike) {
    var base_url = "http://www.datos.gov.co/resource/xax6-k7eu.json";
    var url_find="";
    return {

      // Búsqueda de colegios
      findColegio: function(termino){
        var url_find = "$where=nombreestablecimiento%20like%20'%25"+ termino.toUpperCase() +"%25'";
        var url = base_url+"?"+url_find;

        var promises = $q.defer();
        $http.get(url).then(function(response){
          var data = response.data;
          promises.resolve(data);
        });
        return promises.promise;
      }
    };
  })
  // Mapa GPS
  .factory('MapsGoogle', function($cordovaGeolocation,$ionicModal) {
    // Might use a resource here that returns a JSON array

    var mapId = "", latitud = "", longitud = "";

    return {
      /* 01. Funciòn de inicializaciòn para API de google */
      init: function(MapId, functionError, positionParam) {
        this.mapId = MapId;
        var map  = new google.maps.Map(document.getElementById(MapId), { // Crea el mapa
          zoom: 15,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        });

        var options = {timeout: 10000, enableHighAccuracy: true};
        $cordovaGeolocation.getCurrentPosition(options).then(function(position){
          latitud=positionParam==undefined?position.coords.latitude:positionParam.latitude;
          longitud=positionParam==undefined?position.coords.longitude:positionParam.longitude;
          var locationMark = new google.maps.LatLng(latitud, longitud); // Coordenadas de la localización inicial
          map.setCenter(locationMark);
          // Ubicación actual del dispositivo
          var marker = new google.maps.Marker({
            map: map,
            animation: google.maps.Animation.DROP,
            position: locationMark
          });

        },function(){
          functionError("Debes activar el GPS");
        });
        return map;
      },

      /* 02. Crear las marcas y colca eventos sobre la misma */
      setMarkerMap : function(dataJson, map, href){
        var markers = [];
        // Coordenadas de la localización
        var locationMark = new google.maps.LatLng(dataJson.latitud, dataJson.longitud);
        var marker = new google.maps.Marker({
          position: locationMark,
          map: map,
          icon: "img/ball-icon.png"
        });

        markers.push(marker);

        var content =  '<div class="info-box-wrap padding"><div class="info-box-text-wrap">'+
        '<h4 class="address">'+dataJson.nombre+'</h4>'+
        '<p class="price">Dir. '+dataJson.direccion+'</p>'+
        '<p class="price"> Tel. '+ (dataJson.telefono != undefined ? dataJson.telefono: 'No registra') + '</p>'+
        '</div> <div><br>'+
        '<a class="button button-af" href="'+href+'/'+dataJson.id+'" >Ver información</a>'+
        '</div></div>';


        var ibOptions = {
          disableAutoPan: false
          ,maxWidth: 0
          ,pixelOffset: new google.maps.Size(-140, 0)
          ,zIndex: null
          ,boxStyle: {
            padding: "0px 0px 0px 0px",
            width: "210px",
            height: "40px"
          },
          closeBoxURL : "",
          infoBoxClearance: new google.maps.Size(1, 1),
          isHidden: false,
          pane: "floatPane",
          enableEventPropagation: false
        };



        // Evento click informativo : Texto que se muestra en la burbuja de información
        google.maps.event.addListenerOnce(map, 'idle', function(){
          // Burbuja de información
          var infoWindow = new google.maps.InfoWindow({ content: content });
          // Evento click : Evento de click sobre el ball-icon.png
          google.maps.event.addListener(marker, 'click', function () {

            var boxText = document.createElement("div");
            boxText.style.cssText = "margin-top: 8px; background: #fff; padding: 0px;";
            boxText.innerHTML = content;
            ibOptions.content = boxText
            infoWindow.open(this.map, marker);
            this.map.panTo(infoWindow.getPosition());

          });
        });
      },

      /* 03. Crea elemento de control flotante */
      controlButton: function(controlDiv, map, dataControl){
        // Set CSS for the control border.
        var controlUI = document.createElement('div');
        controlUI.classList.add("button");
        controlUI.classList.add("button-block");
        controlUI.classList.add("button-af");
        controlUI.style.marginBottom = '22px';
        controlDiv.appendChild(controlUI);

        // Set CSS for the control interior.
        var controlText = document.createElement('div');
        controlText.innerHTML = dataControl.titleControl;
        controlUI.appendChild(controlText);

        // Setup the click event listeners: simply set the map to Chicago.
        controlUI.addEventListener('click', dataControl.functionClick);
      },

      /* 04. Agrega a map un botón de control */
      mapControl: function(map, dataControl){
        var centerControlDiv = document.createElement('div');
        var centerControl = this.controlButton(centerControlDiv,map,dataControl);
        centerControlDiv.index = 1;
        map.controls[dataControl.position].push(centerControlDiv);
      }
    };
  })

  ;
